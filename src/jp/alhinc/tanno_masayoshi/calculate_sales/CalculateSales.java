package jp.alhinc.tanno_masayoshi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
    public static void main(String[] args) {

    	Map<String, String> branchNames = new HashMap<>();
    	Map<String, Long> branchSales = new HashMap<>();
    	Map<String, String> commodityNames = new HashMap<>();
    	Map<String, Long> commoditySales = new HashMap<>();

    	if (args.length != 1) {
    		System.out.println("予期せぬエラーが発生しました");
    		return;
    	}
    	if (!putInMap(args[0], "branch.lst", "支店", "^\\d{3}$", branchNames, branchSales)) {
    		return;
    	}
    	if (!putInMap(args[0], "commodity.lst", "商品", "^[A-Za-z0-9]+$", commodityNames, commoditySales)) {
    		return;
    	}

    	File[] files = new File(args[0]).listFiles(); //filesに、指定したパス内の全ファイルの参照を格納
    	List<File> rcdFiles = new ArrayList<>(); //ArrayListを作成
       	for(int i = 0; i < files.length; i++) {
           	if (files[i].getName().matches("^\\d{8}.rcd$") && (files[i].isFile())) {
           		rcdFiles.add(files[i]); //rcdFilesにはif条件に適したファイルが格納される
           	}
       	}

       	Collections.sort(rcdFiles); //昇順でリストをソート
       	for(int i = 0; i < rcdFiles.size() - 1; i++) {
       		int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
       		int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
       		if((latter - former) != 1) {
       			System.out.println("売上ファイル名が連番になっていません");
       			return;
       		}
       	}

       	BufferedReader br2 = null;
        for(int i = 0; i < rcdFiles.size(); i++) {
        	try {
   		        List<String> saleInfo =new ArrayList<>(); //while文で抽出するearningを格納
   				FileReader fr2 = new FileReader(rcdFiles.get(i));
		    	br2 = new BufferedReader(fr2);
		    	String line;
		    	while((line=br2.readLine()) != null) {
		    		saleInfo.add(line); //saleInfo[0]は支店コード、[1]は売上額
		    	}
		    	if (saleInfo.size() != 3) {
		    		System.out.println(rcdFiles.get(i)+"のフォーマットが不正です");
		    		return;
		    	}
		    	if (!branchNames.containsKey(saleInfo.get(0))) {
		    		System.out.println(rcdFiles.get(i)+"の支店コードが不正です");
	    			return;
		    	}
		    	if (!commodityNames.containsKey(saleInfo.get(1))) {
		    		System.out.println(rcdFiles.get(i)+"の商品コードが不正です");
	    			return;
		    	}
		    	if (!saleInfo.get(2).matches("^[0-9]*$")) {
		    		System.out.println("予期せぬエラーが発生しました");
	    			return;
		    	}
		    	long earning = Long.parseLong(saleInfo.get(2));
		    	long sumOfBranch = earning + branchSales.get(saleInfo.get(0));
	    		if (sumOfBranch >= 10000000000L) { //11桁になったらreturn
	    			System.out.println("金額が10桁を超えています");
	    			return;
	    		}
	    		long sumOfCommodity = earning + commoditySales.get(saleInfo.get(1));
	    		if (sumOfCommodity >= 10000000000L) { //11桁になったらreturn
	    			System.out.println("金額が10桁を超えています");
	    			return;
	    		}
	    		branchSales.replace(saleInfo.get(0), sumOfBranch);
	    		commoditySales.replace(saleInfo.get(1), sumOfCommodity);
   			} catch(IOException e) {
   	        	System.out.println("予期せぬエラーが発生しました");
   	        	return;
   	        } finally {
   	        	if(br2 != null) {
   	        		try {
   	        			br2.close(); //brがnull以外なら書き込み状態のままなので、閉じる
   	        		} catch(IOException e) {
   	        			System.out.println("予期せぬエラーが発生しました");
   	        			return;
   	        		}
   	        	}
   	        }
        }

       	if (!outPutFile(args[0], "branch.out", branchNames, branchSales)) {
        	return;
       	}
       	if (!outPutFile(args[0], "commodity.out", commodityNames, commoditySales)) {
        	return;
       	}

    }
    //支店定義ファイル、商品定義ファイルの読み込み
    private static boolean putInMap (String path, String fileName, String fileFormat, String expression, Map<String, String> nameMap, Map<String, Long> saleMap) {
    	File file = new File(path, fileName);
    	if (!file.exists()) {
   			System.out.println(fileFormat +"定義ファイルが存在しません");
   			return false;
   		} else {
   			BufferedReader br = null;
   			try {
   				FileReader fr = new FileReader(file);
   	   			br = new BufferedReader(fr);
   	   			String line;
   	   			while((line=br.readLine()) != null) { //BufferedReaderのreadLineは「一行ずつの読み込み」
   	   				String[] items = line.split(",");
   	   				if((items.length != 2) || !(items[0].matches(expression))) {
   	   					System.out.println(fileFormat + "定義ファイルのフォーマットが不正です");
   	   					return false;
   	   				}
   	   				nameMap.put(items[0], items[1]);
   	   				saleMap.put(items[0], 0L);
   	   			}
   	    	} catch(IOException e) {
   	         	System.out.println("予期せぬエラーが発生しました");
   	         	return false;
   	        } finally { //try内でreturnが選択されても実行される
   	        	if(br != null) {
   	        		try {
   	         			br.close();
   	         		} catch(IOException e) {
   	         			System.out.println("予期せぬエラーが発生しました");
   	         			return false;
   	         		}
   	        	}
   	        }
   			return true;
   		}
    }

    //支店別売上ファイル、賞品別売上ファイルの出力
    private static boolean outPutFile(String path, String fileName, Map<String, String> nameMap, Map<String, Long> saleMap) {
    	BufferedWriter bw = null;
        try {
            File file = new File(path,fileName);
            FileWriter fw = new FileWriter(file);
            bw = new BufferedWriter(fw); //BufferedWriter bwへ代入(左辺をBufferedWriter bwにすると新たなインスタンス変数bwを作成し、重複が発生)
            for(String key : nameMap.keySet()) { //keyはbranchNamesに含まれるキーを順番に代入するもの
        		bw.write(key + "," + nameMap.get(key) + "," + saleMap.get(key));
        		bw.newLine();
    		}
        } catch(IOException e) {
        	System.out.println("予期せぬエラーが発生しました");
        	return false;
        } finally {
        	try {
        		bw.close();
        	} catch(IOException e) {
	        	System.out.println("予期せぬエラーが発生しました");
        	    return false;
        	}
        }
        return true;
    }
}
